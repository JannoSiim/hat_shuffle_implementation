/** @file      utils.cpp
 *****************************************************************************
 * @author     Stefanos Chaliasos
 * @copyright  MIT license (see LICENSE file)
 *****************************************************************************/
#include "utils.hpp"
#include <random>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <gmp.h>
#include "types.hpp"
#include "shuffle_util.hpp"

using namespace std;
using s_cipher = pair<string [4],string [4]>;
using s_ciphers = vector<s_cipher>;

typedef libff::bn128_pp ppT; //Choose elliptic ppT (currently Barreto-Naehrig)

namespace utils {

vector<long> generate_votes(long n) {
    vector <long> votes;
    for (long i = 1; i <= n; i++) {
        votes.push_back(i);
    }
    return votes;
}

vector <string> get_coordinates (libff::G1<ppT> element) {
    to_affine_coordinates(&element);
    vector <string> coordinates;
    for (int i = 0; i < 2; i++) {
        coordinates.push_back(element.coord[i].toString());
    }
    return coordinates;
}

vector <string> get_coordinates (libff::G2<ppT> element) {
    to_affine_coordinates(&element);
    vector <string> coordinates;
    for (int i = 0; i < 2; i++) {
        coordinates.push_back(element.coord[i].a_.toString());
        coordinates.push_back(element.coord[i].b_.toString());
    }
    return coordinates;
}

vector <vector<string>> get_coordinates (vector<libff::G1<ppT>> points) {
    to_affine_coordinates(&points);
    vector <vector<string>> coordinates;
    for (auto point: points) {
        vector <string> coords = get_coordinates(point);
        coordinates.push_back(coords);
    }
    return coordinates;
}

vector <vector<string>> get_coordinates (vector<libff::G2<ppT>> points) {
    to_affine_coordinates(&points);
    vector <vector<string>> coordinates;
    for (auto point: points) {
        vector <string> coords = get_coordinates(point);
        coordinates.push_back(coords);
    }
    return coordinates;
}

libff::G1<ppT> set_coordinates_G1(vector<string> coord) {
    libff::G1<ppT> a;
    a.to_special();
    a.coord[0].set(coord[0]);
    a.coord[1].set(coord[1]);
    a.coord[2] = 1;
    return a;
}

vector<libff::G1<ppT>> set_vector_coordinates_G1(vector<vector<string>> coords)
{
    vector<libff::G1<ppT>> a;
    for(auto coord : coords) {
        libff::G1<ppT> b = set_coordinates_G1(coord);
        a.push_back(b);
    }
    return a;
}

libff::G2<ppT> set_coordinates_G2(vector<string> coord) {
    libff::G2<ppT> a;
    a.coord[0].a_.set(coord[0]);
    a.coord[0].b_.set(coord[1]);
    a.coord[1].a_.set(coord[2]);
    a.coord[1].b_.set(coord[3]);
    a.coord[2] = 1;
    return a;
}

vector<libff::G2<ppT>> set_vector_coordinates_G2(vector<vector<string>> coords)
{
    vector<libff::G2<ppT>> a;
    for(auto coord : coords) {
        libff::G2<ppT> b = set_coordinates_G2(coord);
        a.push_back(b);
    }
    return a;
}

ElGamal_pair<ppT> deserialize_ElGamal_pair (vector<vector<string>> s_ciphertext)
{
    ElGamal_pair<ppT> ciphertext;
    ciphertext.first = set_coordinates_G2(s_ciphertext[0]);
    ciphertext.second = set_coordinates_G2(s_ciphertext[1]);
    ciphertext.first.to_special();
    ciphertext.second.to_special();
    return ciphertext;
}

void to_affine_ElGamal_pair (ElGamal_pair<ppT>* p) {
    p->first.to_special();
    p->second.to_special();
}

ElGamal_vector<ppT> deserialize_ciphertexts(
        vector<vector<vector<string>>> s_ciphertexts) {
    ElGamal_vector<ppT> ciphertexts;
    for (long i = 0; i < s_ciphertexts.size(); i++) {
        ElGamal_pair<ppT> ciphertext = deserialize_ElGamal_pair(
                                           s_ciphertexts[i]);
        ciphertexts.push_back(ciphertext);
    }
    return ciphertexts;
}

void to_affine_ElGamal_vector (ElGamal_vector<ppT>* v) {
    for (auto i = (*v).begin(); i != (*v).end(); ++i) {
        i->first.to_special();
        i->second.to_special();
    }
}

pair<string [4], string [4]> serialize_ElGamal_pair (ElGamal_pair<ppT> p) {
    s_cipher serialized_cipher;
    serialized_cipher.first[0] = p.first.coord[0].a_.toString();
    serialized_cipher.first[1] = p.first.coord[0].b_.toString();
    serialized_cipher.first[2] = p.first.coord[1].a_.toString();
    serialized_cipher.first[3] = p.first.coord[1].b_.toString();
    serialized_cipher.second[0] = p.second.coord[0].a_.toString();
    serialized_cipher.second[1] = p.second.coord[0].b_.toString();
    serialized_cipher.second[2] = p.second.coord[1].a_.toString();
    serialized_cipher.second[3] = p.second.coord[1].b_.toString();
    return serialized_cipher;
}

s_ciphers serialize_ciphertexts (ElGamal_vector<ppT> ciphertexts) {
    vector<pair<string [4], string [4]>> serialized_ciphertexts;
    for (auto p : ciphertexts) {
        s_cipher serialized_cipher = serialize_ElGamal_pair(p);
        serialized_ciphertexts.push_back(serialized_cipher);
    }
    return serialized_ciphertexts;
}

string Fr_toString (libff::Fr<ppT> num) {
    char s[256];
    mpz_t t;
    mpz_init(t);
    num.as_bigint().to_mpz(t);
    mpz_get_str(s, 10, t);
    mpz_clear(t);
    string str (s);
    return str;
}

vector<string> generate_random_votes (long n, libff::G2<ppT> h) {
    vector<string> votes;
    for (long i = 0; i < n; i++) {
        libff::Fr<ppT> vote = libff::Fr<ppT>::random_element();
        string contents = utils::Fr_toString(vote);
        votes.push_back(contents);
    }
    return votes;
}

vector<string> generate_random_votes (long n, long b, libff::G2<ppT> h) {
    vector<string> votes;
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator
    std::uniform_int_distribution<> distr(1, b); // define the range

    for (long i = 0; i < n; i++) {
        long random_vote = distr(eng);
        libff::Fr<ppT> vote = libff::Fr<ppT>(random_vote);
        string contents = utils::Fr_toString(vote);
        votes.push_back(contents);
    }
    return votes;
}

ElGamal_vector<ppT> get_ciphertexts(vector<libff::Fr<ppT>> votes,
                                    libff::G2<ppT> h) {
    auto n = votes.size();
    libff::G2<ppT> g2 = libff::G2<ppT>::one();
    libff::Fr_vector<ppT> randomizers = generate_randomizers<ppT>(n, 256);
    ElGamal_vector<ppT> encrypted_votes;
    for (vector<libff::Fr<ppT>>::size_type i = 0; i != votes.size(); i++) {
        ElGamal_pair<ppT> ciphertext {randomizers.at(i) * g2, (votes[i] * g2) +
                                        (randomizers.at(i) * h)};
        ciphertext.first.to_special();
        ciphertext.second.to_special();
        encrypted_votes.push_back(ciphertext);
        if (i % 25000 == 0) {
            cout << '\n' << i << endl;
        }
    }
    return encrypted_votes;
}

vector<libff::Fr<ppT>> deserialize_votes (vector<string> d_votes) {
    vector<libff::Fr<ppT>> votes;
    for(auto const& value: d_votes) {
        char input[1024];
        strcpy(input, value.c_str());
        libff::Fr<ppT> vote = libff::Fr<ppT>(input);
        votes.push_back(vote);
    }
    return votes;
}

vector<long> generate_perm(long n) {
    vector<long> permutation;
    for (long i = 0; i < n; i++) permutation.push_back(i);
    // uses default random generator!
    random_shuffle(permutation.begin(), permutation.end());
    return permutation;
}

string get_key_for_ciphertext (libff::G2<ppT> ciphertext) {
    return ciphertext.coord[0].a_.toString() +
           ciphertext.coord[0].b_.toString() +
           ciphertext.coord[1].a_.toString() +
           ciphertext.coord[1].b_.toString();
}

map<string, libff::Fr<ppT>> make_table(vector<libff::Fr<ppT>> votes) {

    libff::G2<ppT> g2 = libff::G2<ppT>::one();

    map<string, libff::Fr<ppT>> table;
    for (auto vote : votes) {
        libff::G2<ppT> elem = vote * g2;
        elem.to_special();
        string s = get_key_for_ciphertext(elem);
        table[s] = vote;
    }
    return table;
}

vector<string> decrypt_ciphers (ElGamal_vector<ppT> ciphertexts,
                                libff::Fr<ppT> secret,
                                map<string, libff::Fr<ppT>> table) {
    vector<string> decrypted_votes;
    for (auto p_ciphertext : ciphertexts) {
        libff::G2 <ppT> v = p_ciphertext.second +
            (-secret * p_ciphertext.first);
        v.to_special();
        string key = get_key_for_ciphertext(v);
        string d_vote = Fr_toString (table[key]);
        decrypted_votes.push_back(d_vote);
    }
    return decrypted_votes;
}

ElGamal_vector<ppT> generate_ciphertexts(long n, libff::G2<ppT> h) {
    libff::G2<ppT> g2 = libff::G2<ppT>::one();
    libff::Fr_vector<ppT> randomizers = generate_randomizers<ppT>(n, 256);
    ElGamal_vector<ppT> input;
    for (long i = 0; i < n; i++) {
        libff::Fr<ppT> msg = libff::Fr<ppT>::random_element();
        ElGamal_pair<ppT> ciphertext {randomizers.at(i) * g2, (msg * g2) +
                                        (randomizers.at(i) * h)};
        input.push_back(ciphertext);
    }

    //There is a faster version with mixed addition (can it be used?)

    return input;
    }

} // utils
