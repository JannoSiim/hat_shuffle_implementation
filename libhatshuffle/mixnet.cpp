/** @file      utils.hpp
 *****************************************************************************
 * @author     Stefanos Chaliasos
 * @copyright  MIT license (see LICENSE file)
 *****************************************************************************/
#include <iostream>
#include <vector>
#include <map>
#include <sstream>
#include <algorithm> //has random_shuffle
#include <libff/algebra/fields/bigint.hpp>
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <libff/algebra/curves/public_params.hpp>
#include <fstream>
#include <string>
#include "types.hpp"
#include "crs.hpp"
#include "utils.hpp"
#include "prover.hpp"
#include "verifier.hpp"
#include "shuffle_util.hpp"
#include "nlohmann/json.hpp"

using namespace std;
using json = nlohmann::json;
using s_cipher = pair<string [4],string [4]>;
using s_ciphers = vector<s_cipher>;

typedef libff::bn128_pp curve;

json serialiaze_proof (Proof<curve> &proof) {
    json s_proof;
    utils::to_affine_ElGamal_pair(&proof.second.consist);
    utils::to_affine_ElGamal_vector(&proof.second.output);
    json offline = {{"a_coms", utils::get_coordinates(proof.first.a_coms)},
                    {"a_hat_coms",
                        utils::get_coordinates(proof.first.a_hat_coms)},
                    {"b_coms", utils::get_coordinates(proof.first.b_coms)},
                    {"uvs", utils::get_coordinates(proof.first.uvs)},
                    {"same_msgs",
                        utils::get_coordinates(proof.first.same_msgs)},
                    {"t_com", utils::get_coordinates(proof.first.t_com)}};
    json online = {{"output",
                        utils::serialize_ciphertexts(proof.second.output)},
                   {"consist",
                        utils::serialize_ElGamal_pair(proof.second.consist)}};
    s_proof = {{"Offline_proof", offline},
               {"Online_proof", online}};
    return s_proof;
}

bool key_gen(long n, string public_file, string secret_file) {
    curve::init_public_params();

    libff::Fr<curve> sk = libff::Fr<curve>::random_element();

    libff::G2<curve> g2 = libff::G2<curve>::one();

    // Precomputation for fixed-based multi-exponentiation of g2.
    const size_t g2_exp_count = n + 6;
    size_t g2_window_size = libff::get_exp_window_size<libff::G2<curve>>(
                                g2_exp_count);
    libff::window_table<libff::G2<curve>> g2_table = get_window_table(
        libff::Fr<curve>::size_in_bits(),
        g2_window_size, g2);

    libff::G2<curve> pk = windowed_exp(libff::Fr<curve>::size_in_bits(),
                                       g2_window_size,
                                       g2_table, sk);

    // write sk
    string sk_s = utils::Fr_toString(sk);
    std::ofstream s(secret_file);
    s << sk_s;
    s.close();

    // write pk
    vector <string> pk_s = utils::get_coordinates(pk);
    json pk_json;
    pk_json["pk"] = pk_s;
    std::ofstream out(public_file);
    out << std::setw(4) << pk_json << std::endl;

    return true;
}

bool create_crs(long votes_number, string crs_file, string public_file) {
    curve::init_public_params();

    // deserialize pk
    std::ifstream j(public_file);
    json pk_j;
    j >> pk_j;

    vector<string> pk_s = pk_j["pk"];
    libff::G2<curve> pk = utils::set_coordinates_G2(pk_s);

    // Create crs
    CRS<curve> crs {votes_number, pk};

    libff::enter_block("CRS Serialization", true);

    // crs.print ();
    crs.to_affine ();

    // serialize
    json crs_json;
    crs_json["crs"] = crs.serialize();

    // Write to JSON files
    std::ofstream out(crs_file);
    out << std::setw(4) << crs_json << std::endl;

    libff::leave_block("CRS Serialization", true);

    return true;
}

bool demo_voting(long votes_number, long encoding_number, string public_file,
                 string votes_file) {
    curve::init_public_params();

    // deserialize pk
    std::ifstream j(public_file);
    json pk_j;
    j >> pk_j;

    vector<string> pk_s = pk_j["pk"];
    libff::G2<curve> pk = utils::set_coordinates_G2(pk_s);

    // Create random encoded votes
    vector<string> r_votes = utils::generate_random_votes(votes_number,
                                                          encoding_number,
                                                          pk);

    // serialize
    json votes_json;
    votes_json["votes"] = r_votes;

    // Write to JSON files
    std::ofstream out(votes_file);
    out << std::setw(4) << votes_json << std::endl;

    return true;
}

bool generate_encoded_votes(string crs_file, string votes_file) {
    curve::init_public_params();

    // Read from json files
    std::ifstream j(crs_file);
    json crs_j;
    j >> crs_j;

    // deserialize
    long n = crs_j["crs"]["n"];
    vector<string> g2_sk = crs_j["crs"]["g2_sk"];
    libff::G2<curve> sk = utils::set_coordinates_G2(g2_sk);

    // Create random encoded votes
    vector<string> r_votes = utils::generate_random_votes(n, sk);

    // serialize
    json votes_json;
    votes_json["votes"] = r_votes;

    // Write to JSON files
    std::ofstream out(votes_file);
    out << std::setw(4) << votes_json << std::endl;

    return true;
}

bool encrypt(string crs_file, string votes_file, string ciphertexts_file) {
    curve::init_public_params();

    // Read from json files
    libff::enter_block("Votes deserialization", true);
    std::ifstream i(votes_file);
    json votes_j;
    i >> votes_j;
    vector<string> s_votes = votes_j["votes"];
    vector<libff::Fr<curve>> votes = utils::deserialize_votes(s_votes);
    libff::leave_block("Votes deserialization", true);

    libff::enter_block("CRS deserialization", true);
    std::ifstream j(crs_file);
    json crs_j;
    j >> crs_j;
    vector<string> g2_sk = crs_j["crs"]["g2_sk"];
    libff::G2<curve> sk = utils::set_coordinates_G2(g2_sk);
    libff::leave_block("CRS deserialization", true);


    // encrypt
    libff::enter_block("Encryption", true);
    ElGamal_vector<curve> ciphertexts = utils::get_ciphertexts(votes, sk);
    libff::leave_block("Encryption", true);

    // serialize
    libff::enter_block("Ciphertexts Serialization", true);
    s_ciphers serialized_ciphertexts = utils::serialize_ciphertexts(ciphertexts);

    // Write to json file
    json encrypted;
    encrypted["ciphertexts"] = serialized_ciphertexts;

    std::ofstream out(ciphertexts_file);
    out << std::setw(4) << encrypted << std::endl;
    libff::leave_block("Ciphertexts Serialization", true);

    return true;
}

bool prove (string crs_file, string ciphertexts_file, string proofs_file) {

    curve::init_public_params();

    // Read from JSON file
    libff::enter_block("CRS deserialization", true);
    std::ifstream i(crs_file);
    json j;
    i >> j;

    long n = j["crs"]["n"];

    // Create - deserialize crs
    CRS<curve>* crs = new  CRS<curve>{n, j["crs"]};
    libff::leave_block("CRS deserialization", true);
    j.clear();

    // Read from JSON file
    libff::enter_block("Ciphertexts deserialization", true);
    std::ifstream x(ciphertexts_file);
    json y;
    x >> y;

    // Deserialize ciphertexts
    ElGamal_vector<curve> ciphertexts = utils::deserialize_ciphertexts(y["ciphertexts"]);
    y.clear();
    libff::leave_block("Ciphertexts deserialization", true);

    // prove
    vector<long> permutation = utils::generate_perm(n);
    libff::Fr_vector<curve> randomizers_cipher = generate_randomizers<curve>(
                                                     n, 256);
    libff::Fr_vector<curve> randomizers_proof = generate_randomizers<curve>(
                                                    n - 1, 256);
    // CHANGES
    libff::Fr_vector<curve> hat_randomizers_proof = generate_randomizers<curve>(
                                                    n - 1, 256);
    libff::Fr<curve> randomizer_t = libff::Fr<curve>::random_element();
    Prover<curve> prover {*crs};
    Proof<curve> proof = prover.prove(ciphertexts, permutation,
                                      randomizers_cipher, randomizers_proof,
                                      hat_randomizers_proof,
                                      randomizer_t);
    proof = prover.to_affine_proof();

    ciphertexts.clear();
    delete crs;

    // Serialize
    libff::enter_block("Proofs serialization", true);
    json serialized_proof = serialiaze_proof (proof);

    // Wrtie to JSON file
    std::ofstream out(proofs_file);
    out << std::setw(4) << serialized_proof << std::endl;
    libff::leave_block("Proofs seserialization", true);

    return true;
}

bool verify(string crs_file, string ciphertexts_file, string proofs_file) {
    curve::init_public_params();

    // Read from JSON file
    libff::enter_block("CRS deserialization", true);
    std::ifstream i(crs_file);
    json j;
    i >> j;

    long n = j["crs"]["n"];

    // Deserialize crs
    CRS<curve>* crs = new  CRS<curve>{n, j["crs"]};
    libff::leave_block("CRS deserialization", true);
    j.clear();

    // Read from JSON file
    libff::enter_block("Ciphertexts deserialization", true);
    std::ifstream x(ciphertexts_file);
    json y;
    x >> y;

    // Deserialize ciphertexts
    ElGamal_vector<curve> ciphertexts =
        utils::deserialize_ciphertexts(y["ciphertexts"]);
    libff::leave_block("Ciphertexts deserialization", true);
    y.clear();

    // Initialize Prover
    Prover<curve> prover {*crs};

    // Initialize Verifier
    Verifier<curve> verifier {*crs, 40};

    delete crs;

    // Deserialize Proofs
    // Read from JSON file
    libff::enter_block("Proofs deserialization", true);
    std::ifstream k(proofs_file);
    json z;
    k >> z;
    Proof<curve> proof = prover.deserialize(z);
    libff::leave_block("Proofs deserialization", true);

    if (verifier.verify(ciphertexts, proof)) {
        return true;
    }
    return false;
}

bool decrypt(string crs_file, string votes_file, string proofs_file,
             string decrypted_votes_file, string secret_file) {
    curve::init_public_params();

    std::ifstream k(secret_file);
    string secret;
    k >> secret;

    // Read from json files
    libff::enter_block("Votes deserialization", true);
    std::ifstream i(votes_file);
    json votes_j;
    i >> votes_j;
    // deserialize
    vector<string> s_votes = votes_j["votes"];
    votes_j.clear();
    vector<libff::Fr<curve>> votes = utils::deserialize_votes(s_votes);
    libff::leave_block("Votes deserialization", true);

    // make table
    libff::enter_block("Create table", true);
    map<string, libff::Fr<curve>> table = utils::make_table(votes);
    libff::leave_block("Create table", true);

    // std::ifstream x("ciphertexts.json");
    std::ifstream x(proofs_file);
    json ciphertexts_json;
    x >> ciphertexts_json;

    // Deserialize ciphertexts
    libff::enter_block("Ciphertexts deserialization", true);
    ElGamal_vector<curve> ciphertexts = utils::deserialize_ciphertexts(
        ciphertexts_json["Online_proof"]["output"]);
    ciphertexts_json.clear();
    libff::leave_block("Ciphertexts deserialization", true);

    // Get secret manually.
    char input[1024];
    strcpy(input, secret.c_str());
    libff::Fr<curve> s = libff::Fr<curve>(input);

    libff::enter_block("Decryption", true);
    vector<string> decrypted_v = utils::decrypt_ciphers(ciphertexts, s, table);
    libff::leave_block("Decryption", true);

    // Write to json file
    libff::enter_block("Votes serialization", true);
    json decrypted;
    decrypted["votes"] = decrypted_v;

    std::ofstream out(decrypted_votes_file);
    out << std::setw(4) << decrypted << std::endl;
    libff::leave_block("Votes serialization", true);

    return true;
}

bool test_mixnet(long n){
    curve::init_public_params();

    libff::Fr<curve> sk = libff::Fr<curve>::random_element();

    libff::G2<curve> g2 = libff::G2<curve>::one();

    // Precomputation for fixed-based multi-exponentiation of g2.
    const size_t g2_exp_count = n + 6;
    size_t g2_window_size = libff::get_exp_window_size<libff::G2<curve>>(
                                g2_exp_count);
    libff::window_table<libff::G2<curve>> g2_table = get_window_table(
                                            libff::Fr<curve>::size_in_bits(),
                                            g2_window_size, g2);

    libff::G2<curve> pk = windowed_exp(libff::Fr<curve>::size_in_bits(),
                                       g2_window_size,
                                       g2_table,
                                       sk);
    CRS<curve> crs {n, pk};

    ElGamal_vector<curve> ciphertexts = utils::generate_ciphertexts(n,
                                                                    crs.g2_sk);

    vector<long> permutation = utils::generate_perm(n);
    libff::Fr_vector<curve> randomizers_cipher = generate_randomizers<curve>(
                                                   n, 256);
    libff::Fr_vector<curve> randomizers_proof = generate_randomizers<curve>(
                                                   n - 1, 256);
    libff::Fr_vector<curve> hat_randomizers_proof = generate_randomizers<curve>(
                                                    n - 1, 256);
    libff::Fr<curve> randomizer_t = libff::Fr<curve>::random_element();

    Prover<curve> prover {crs};
    libff::enter_block("Protocol", true);
    Proof<curve> proof = prover.prove(ciphertexts, permutation,
                                    randomizers_cipher, randomizers_proof,
                                    hat_randomizers_proof,
                                    randomizer_t);

    Verifier<curve> verifier {crs, 40};
    cout << "Verification = " << verifier.verify(ciphertexts, proof) << endl;
    libff::leave_block("Protocol", true);
    return true;
}
