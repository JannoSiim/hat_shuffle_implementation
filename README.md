## README ##

This is a C++ implementation of a non-interactive zero-knowledge
shuffle argument presented in [An Efficient Pairing-Based Shuffle
Argument](https://eprint.iacr.org/2017/894). The implementation is based
on the [libff library](https://github.com/scipr-lab/libff). Feel free
to experiment with the code, but note that **this implementation is
meant as a prototype and should not be directly used in real life
applications**.

### Overview ###

Shuffling is a process of randomly permuting and blinding
(rerandomizing) a set of ciphertexts. A zero-knowledge shuffle
argument allows a party to prove that it did the shuffle procedure
correctly, without leaking any information about the permutation. A
non-interactive shuffle argument allows the prover to send out a
single message that can be verified by anyone, even if the original
prover is not available anymore.

The main application for a shuffle argument is a mix network (mixnet)
that takes in a set of ciphertexts and lets servers shuffle the
ciphertexts one after another to destroy the link between a ciphertext
and its source. To guarantee that servers do not inject their own
ciphertexts, each server can provide a zero-knowledge proof. Mix
networks are used for e-voting and many other applications that need
to provide ciphertext anonymity.

Many efficient non-interactive shuffle arguments are known that
guarantee security in the random oracle model (ROM). This
unfortunately gives only a heuristic security (cases are known where a
ROM proof does not guarantee security in real life). The shuffle
argument mentioned above is the most efficient construction so far
that avoids ROM. It uses the ElGamal cryptosystem.

### How to set it up ###

#### Linux Ubuntu 16.10 ####

1. Start by recursively fetching the dependencies:

	```bash
	git submodule update --init --recursive
	```

2. Install libff dependecies:

	```bash
    sudo apt-get install build-essential git libboost-all-dev cmake libgmp3-dev libssl-dev libprocps4-dev pkg-config
	```

3. Next, initialize the `build` directory:

	```bash
	mkdir build && cd build && cmake ..
	```

4. Lastly, compile the library:

	```bash
	make
	```

5. To run the application, use the following command from the `build` directory:

	```bash
	./test/program 1000
	```

If the build directory is initialized with `cmake -DMULTICORE=ON ..`,
then the most expensive computations are parallelized.
This however needs some modification of libff library
(Pairings use some static profiling code that does not allow parallelization).
To fix the issue, add code from the folder libff_changes to the libff
folder depends/libff/libff/algebra/curves/bn128/.
After that libff should be recompiled and reinstalled.

#### macOS ####

The LLVM toolkit that comes with XCode in macOS does not include
everything needed to build the code. In order to do so, you must
download a recent version of LLVM, install it, and tell CMake to use
the newly installed LLVM instead of the default.

LLVM is available through Homebrew, but it may take a very long time
to install. You can use the [pre-built
binaries](http://releases.llvm.org/download.html) to save time.

Moreover, the libprocps library does not exist on macOS, so it should
be taken out of the build.

Taking these into account, if you have installed `clang` in
`/usr/local/clang`, then in step 3 above you should invoke CMake as
follows:

```bash
cmake -DWITH_PROCPS=OFF -DCMAKE_CXX_COMPILER=/usr/local/clang/bin/clang++ -DCMAKE_CXX_FLAGS=-isystem\ /usr/local/clang/include/ ..
```

If you have installed `clang` somewhere else, substitute the correct
locations.

Upon building you may discover that the compiler complains that it
cannot find some standard header files. This may occur if they are
missing from `/usr/include`, which is the case in macOS 10.14. To fix
the problem, you will need to run:

```
/Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_10.14.pkg
```

