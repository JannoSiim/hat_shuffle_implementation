#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm> //has random_shuffle
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <libff/algebra/curves/public_params.hpp>
#include <fstream>
#include "crs.hpp"
#include "prover.hpp"
#include "verifier.hpp"
#include "shuffle_util.hpp"
#include "nlohmann/json.hpp"
#include "utils.hpp"

#include <unistd.h>

using namespace std;
using json = nlohmann::json;

typedef libff::bn128_pp curve; //Choose elliptic curve (currently Barreto-Naehrig)

/*!Generates a permutation vector containing numbers 0 to n - 1 in random order.
* Current implementation uses default c++ random generator.
* This is probably not suitable for crypto.
* \param n length of the permutation vector
* \return permutation vector
*/
vector<int> generate_perm(int n) {
  vector<int> permutation;
  for (int i = 0; i < n; i++) permutation.push_back(i);
  random_shuffle(permutation.begin(), permutation.end()); //uses default random generator!
  return permutation;
}

/*! Generates n ElGamal ciphertexts. Encrypted messages are random.
* \param n number of ciphertexts
* \param h ElGamal public key
* \returns ElGamal ciphertexts
*/
ElGamal_vector<curve> generate_ciphertexts(int n, libff::G2<curve> h) {
  libff::G2<curve> g2 = libff::G2<curve>::one();
  libff::Fr_vector<curve> randomizers = generate_randomizers<curve>(n, 256);
  ElGamal_vector<curve> input;
  for (int i = 0; i < n; i++) {
    libff::Fr<curve> msg = libff::Fr<curve>::random_element();
    ElGamal_pair<curve> ciphertext {randomizers.at(i) * g2, (msg * g2) + (randomizers.at(i) * h)};
    input.push_back(ciphertext);
  }

  //There is a faster version with mixed addition (can it be used?)

  return input;
}

vector<string> generate_random_votes (int n, libff::G2<curve> h) {
    vector<string> votes;
    libff::G2<curve> g2 = libff::G2<curve>::one();
    libff::Fr_vector<curve> randomizers = generate_randomizers<curve>(n, 256);
    for (int i = 0; i < n; i++) {
        libff::Fr<curve> vote = libff::Fr<curve>::random_element();

        int stdout_fd = dup(STDOUT_FILENO);
        freopen("bigint", "w", stdout);
        vote.as_bigint().print();
        fclose(stdout);
        dup2(stdout_fd, STDOUT_FILENO);
        stdout = fdopen(STDOUT_FILENO, "w");
        close(stdout_fd);

        string contents;
        ifstream infile("bigint");
        if (infile.good())
        {
            getline(infile, contents);
        }
        infile.close();

        votes.push_back(contents);
    }
    return votes;
}

ElGamal_vector<curve> get_ciphertexts(vector<libff::Fr<curve>> votes,
        libff::G2<curve> h) {
  auto n = votes.size();
  libff::G2<curve> g2 = libff::G2<curve>::one();
  libff::Fr_vector<curve> randomizers = generate_randomizers<curve>(n, 256);
  ElGamal_vector<curve> encrypted_votes;
  for (vector<libff::Fr<curve>>::size_type i = 0; i != votes.size(); i++) {
    ElGamal_pair<curve> ciphertext {randomizers.at(i) * g2, (votes[i] * g2) +
        (randomizers.at(i) * h)};
    ciphertext.first.to_special();
    ciphertext.second.to_special();
    encrypted_votes.push_back(ciphertext);
  }
  return encrypted_votes;
}

vector<libff::Fr<curve>> deserialize_votes (vector<string> d_votes) {
    vector<libff::Fr<curve>> votes;
    for(auto const& value: d_votes) {
        char input[1024];
        strcpy(input, value.c_str());
        libff::Fr<curve> vote = libff::Fr<curve>(input);
        votes.push_back(vote);
    }
    return votes;
}

int main(int argc, char *argv[])
{
  if (argc < 2)
    cerr << "Input parameter missing!" << '\n';

  istringstream ss(argv[1]);
  int n; //number of ciphertexts
  if (!(ss >> n))
    cerr << "Invalid number " << argv[1] << '\n';

  CRS<curve> crs {n};

  vector<int> permutation = generate_perm(n);
  libff::Fr_vector<curve> randomizers_cipher = generate_randomizers<curve>(n, 256);
  libff::Fr_vector<curve> randomizers_proof = generate_randomizers<curve>(n - 1, 256);
  libff::Fr<curve> randomizer_t = libff::Fr<curve>::random_element();

  vector<string> r_votes = generate_random_votes (n, crs.g2_sk);

  json votes_json = {"votes", r_votes};

  vector<string> s_votes = votes_json[1];

  vector<libff::Fr<curve>> votes = deserialize_votes(s_votes);

  ElGamal_vector<curve> ciphertexts1 = get_ciphertexts (votes, crs.g2_sk);

  s_ciphers serialized_ciphertexts = utils::serialize_ciphertexts (ciphertexts1);

  json encrypted = {"ciphertexts", serialized_ciphertexts};

  ElGamal_vector<curve> ciphertexts = utils::deserialize_ciphertexts(encrypted[1]);

  Prover<curve> prover {crs};
  libff::enter_block("Protocol", true);
  Proof<curve> proof = prover.prove(ciphertexts, permutation, randomizers_cipher, randomizers_proof, randomizer_t);

  Verifier<curve> verifier {crs, 40};
  cout << "Verification = " << verifier.verify(ciphertexts, proof) << endl;
  libff::leave_block("Protocol", true);
  return 0;
}
