#include <iostream>
#include <fstream>
#include <sstream>
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <libff/algebra/curves/public_params.hpp>
#include <string>
#include <gmp.h>
#include "utils.hpp"

using namespace std;

typedef libff::bn128_pp curve; //Choose elliptic curve (currently Barreto-Naehrig)

int main() {
    curve::init_public_params();
    libff::G2<ppT> g2 = libff::G2<ppT>::one();
    std::ofstream out("g2_string");
    for (int i = 0; i < 1000000; i++) {
        libff::Fr<curve> number = libff::Fr<curve>(i);
        libff::G2<curve> elem = number * g2;
        if (!elem.is_well_formed())
            cout << "ERROR" << endl;
        string input = utils::get_key_for_ciphertext(elem);
        out << input << '\n';
    }
    out.close();
    return 0;
}
