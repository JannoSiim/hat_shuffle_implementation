#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm> //has random_shuffle
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <libff/algebra/curves/public_params.hpp>
#include <fstream>
#include "mixnet.h"

using namespace std;
using json = nlohmann::json;

typedef libff::bn128_pp curve; //Choose elliptic curve (currently Barreto-Naehrig)

int main(int argc, char *argv[])
{
    if (argc < 2)
        cerr << "Input parameter missing!" << '\n';

    istringstream ss(argv[1]);
    int n; //number of ciphertexts
    if (!(ss >> n))
        cerr << "Invalid number " << argv[1] << '\n';

    test_mixnet(n);
return 0;
}

