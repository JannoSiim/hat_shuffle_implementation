add_executable(
  test_crs ${SOURCES}

  test_crs.cpp
)
target_link_libraries(
  test_crs

  ff hatshuffle
)
target_include_directories(
  test_crs

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  test_key_gen ${SOURCES}

  test_key_gen.cpp
)
target_link_libraries(
  test_key_gen

  ff hatshuffle
)
target_include_directories(
  test_key_gen

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  test_votes_gen ${SOURCES}

  test_votes_gen.cpp
)
target_link_libraries(
  test_votes_gen

  ff hatshuffle
)
target_include_directories(
  test_votes_gen

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  test_encryption ${SOURCES}

  test_encryption.cpp
)
target_link_libraries(
  test_encryption

  ff hatshuffle
)
target_include_directories(
  test_encryption

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  test_case_bigint ${SOURCES}

  test_case_bigint.cpp
)
target_link_libraries(
  test_case_bigint

  ff hatshuffle
)
target_include_directories(
  test_case_bigint

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  test_mapping ${SOURCES}

  test_mapping.cpp
)
target_link_libraries(
  test_mapping

  ff hatshuffle
)
target_include_directories(
  test_mapping

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  test_prove ${SOURCES}

  test_prove.cpp
)
target_link_libraries(
  test_prove

  ff hatshuffle
)
target_include_directories(
  test_prove

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  test_verify ${SOURCES}

  test_verify.cpp
)
target_link_libraries(
  test_verify

  ff hatshuffle
)
target_include_directories(
  test_verify

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  test_decrypt ${SOURCES}

  test_decrypt.cpp
)
target_link_libraries(
  test_decrypt

  ff hatshuffle
)
target_include_directories(
  test_decrypt

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)

add_executable(
  program ${SOURCES}

  program.cpp
)
target_link_libraries(
  program

  ff hatshuffle
)
target_include_directories(
  program

  PUBLIC
  ${DEPENDS_DIR}/libff
  ${DEPENDS_DIR}/libff/depends/ate-pairing
  ${LIB_DIR}
)
