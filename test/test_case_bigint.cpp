#include <iostream>
#include <sstream>
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <libff/algebra/curves/public_params.hpp>
#include <string>
#include <gmp.h>
#include "utils.hpp"

using namespace std;

typedef libff::bn128_pp curve; //Choose elliptic curve (currently Barreto-Naehrig)

int main() {
   curve::init_public_params();

   libff::Fr<curve> elem = libff::Fr<curve>::random_element();
   elem.print();
   elem.as_bigint().print();

   char str[256];
   mpz_t t;
   mpz_init(t);
   elem.as_bigint().to_mpz(t);
   mpz_get_str(str, 10, t);
   mpz_clear(t);
   cout << str << endl;

   cout << utils::Fr_toString(elem) << endl;

   // cout << elem;

   // std::stringstream ss;
   // ss << elem.as_bigint();
   // std::string s = ss.str();
   // std::cout << s << '\n';

   return 0;
}
