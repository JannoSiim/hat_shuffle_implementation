#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm> //has random_shuffle
#include <libff/algebra/curves/bn128/bn128_pp.hpp>
#include <libff/algebra/curves/public_params.hpp>
#include <fstream>
#include "crs.hpp"
#include "prover.hpp"
#include "verifier.hpp"
#include "shuffle_util.hpp"
#include "nlohmann/json.hpp"
#include "utils.hpp"

#include <unistd.h>

using namespace std;
using json = nlohmann::json;

typedef libff::bn128_pp curve; //Choose elliptic curve (currently Barreto-Naehrig)

/*!Generates a permutation vector containing numbers 0 to n - 1 in random order.
* Current implementation uses default c++ random generator.
* This is probably not suitable for crypto.
* \param n length of the permutation vector
* \return permutation vector
*/
vector<int> generate_perm(int n) {
  vector<int> permutation;
  for (int i = 0; i < n; i++) permutation.push_back(i);
  random_shuffle(permutation.begin(), permutation.end()); //uses default random generator!
  return permutation;
}

/*! Generates n ElGamal ciphertexts. Encrypted messages are random.
* \param n number of ciphertexts
* \param h ElGamal public key
* \returns ElGamal ciphertexts
*/
ElGamal_vector<curve> generate_ciphertexts(int n, libff::G2<curve> h) {
  libff::G2<curve> g2 = libff::G2<curve>::one();
  libff::Fr_vector<curve> randomizers = generate_randomizers<curve>(n, 256);
  ElGamal_vector<curve> input;
  for (int i = 0; i < n; i++) {
    libff::Fr<curve> msg = libff::Fr<curve>::random_element();
    ElGamal_pair<curve> ciphertext {randomizers.at(i) * g2, (msg * g2) + (randomizers.at(i) * h)};
    input.push_back(ciphertext);
  }

  //There is a faster version with mixed addition (can it be used?)

  return input;
}

json serialiaze_proof (Proof<curve> proof) {
    json s_proof;
    utils::to_affine_ElGamal_pair (&proof.second.consist);
    utils::to_affine_ElGamal_vector (&proof.second.output);
    json offline = {{"a_coms", utils::get_coordinates(proof.first.a_coms)},
                    {"a_hat_coms", utils::get_coordinates(proof.first.a_hat_coms)},
                    {"b_coms", utils::get_coordinates(proof.first.b_coms)},
                    {"uvs", utils::get_coordinates(proof.first.uvs)},
                    {"same_msgs", utils::get_coordinates(proof.first.same_msgs)},
                    {"t_com", utils::get_coordinates(proof.first.t_com)}};
    json online = {{"output", utils::serialize_ciphertexts(proof.second.output)},
                   {"consist", utils::serialize_ElGamal_pair(proof.second.consist)}};
    s_proof = {{"Offline_proof", offline},
               {"Online_proof", online}};
    return s_proof;
}

int main(int argc, char *argv[])
{
  if (argc < 2)
    cerr << "Input parameter missing!" << '\n';

  istringstream ss(argv[1]);
  int n; //number of ciphertexts
  if (!(ss >> n))
    cerr << "Invalid number " << argv[1] << '\n';

  CRS<curve> crs {n};

  ElGamal_vector<curve> ciphertexts = generate_ciphertexts(n, crs.g2_sk);

  vector<int> permutation = generate_perm(n);
  libff::Fr_vector<curve> randomizers_cipher = generate_randomizers<curve>(n, 256);
  libff::Fr_vector<curve> randomizers_proof = generate_randomizers<curve>(n - 1, 256);
  libff::Fr<curve> randomizer_t = libff::Fr<curve>::random_element();

  Prover<curve> prover {crs};
  libff::enter_block("Protocol", true);
  Proof<curve> proof1 = prover.prove(ciphertexts, permutation, randomizers_cipher, randomizers_proof, randomizer_t);

  json serialized_proof = serialiaze_proof (proof1);

  Proof<curve> proof = prover.deserialize(serialized_proof);

  cout << "############" << endl;
  cout << "t_com: " << (proof1.first.t_com == proof.first.t_com) << endl;
  cout << "a_coms: " << (proof1.first.a_coms == proof.first.a_coms) << endl;
  cout << "a_hat_coms: " << (proof1.first.a_hat_coms == proof.first.a_hat_coms) << endl;
  cout << "b_coms: " << (proof1.first.b_coms == proof.first.b_coms) << endl;
  cout << "uvs: " << (proof1.first.uvs == proof.first.uvs) << endl;
  cout << "same_msgs: " << (proof1.first.same_msgs == proof.first.same_msgs) << endl;
  cout << "output: " << (proof1.second.output == proof.second.output) << endl;
  cout << "consist: " << (proof1.second.consist == proof.second.consist) << endl;
  cout << "############" << endl;

  Verifier<curve> verifier {crs, 40};
  cout << "Verification = " << verifier.verify(ciphertexts, proof) << endl;
  libff::leave_block("Protocol", true);
  return 0;
}

